import { Static } from '@sinclair/typebox'
import { commandSchema, incidentSchema } from './schemas'

export type Command = Static<typeof commandSchema>
export type Incident = Static<typeof incidentSchema>

export enum CmdsIn {
  REGISTER = 'register',
  UNREGISTER = 'unregister',
}

export enum CmdsOut {
  INCIDENTS = 'incidents',
}
