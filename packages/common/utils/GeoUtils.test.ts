import { haversineDistance } from './GeoUtils'
import { describe, expect, it } from 'vitest'

describe('GeoUtils tests', () => {
  it('haversine distance ARN->MMX', () => {
    const arnLat = 59.65014
    const arnLon = 17.94363
    const mmxLat = 55.52888
    const mmxLon = 13.37111

    const distance = Math.round(
      haversineDistance(arnLat, arnLon, mmxLat, mmxLon)
    )

    expect(distance).to.equal(533)
  })
})
