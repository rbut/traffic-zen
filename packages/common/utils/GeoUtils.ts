const EARTH_RADIUS = 6371

function toRad(a: number): number {
  return (a * Math.PI) / 180
}

/**
 * Calculates the distance between two points
 * using Haversine formula.
 *
 * @param lat1 - latitude to point 1.
 * @param lon1 - longitude to point 1.
 * @param lat2 - latitude to point 2.
 * @param lon2 - longitude to point 2.
 * @returns distance between the points.
 */
export function haversineDistance(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
): number {
  const x1 = lat2 - lat1
  const dLat = toRad(x1)

  const x2 = lon2 - lon1
  const dLon = toRad(x2)

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRad(lat1)) *
      Math.cos(toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  const d = EARTH_RADIUS * c

  return d
}
