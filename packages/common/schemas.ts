import { Type } from '@sinclair/typebox'

export const commandSchema = Type.Object({
  cmd: Type.String(),
  value: Type.Any(),
})

export const incidentSchema = Type.Object(
  {
    id: Type.Integer(),
    priority: Type.Integer(),
    createddate: Type.String(),
    title: Type.String(),
    exactlocation: Type.String(),
    description: Type.String(),
    latitude: Type.Number(),
    longitude: Type.Number(),
    category: Type.Integer(),
    subcategory: Type.String(),
  },
  { additionalProperties: false }
)

export const incidentsSchema = Type.Array(incidentSchema)
