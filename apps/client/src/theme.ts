const theme = {
  global: {
    font: {
      family: 'Quicksand',
      size: '1.8rem',
      height: '2rem',
    },
  },
}

export default theme
