import { Header, Heading, Nav, Button } from 'grommet'
import { Configure } from 'grommet-icons'
import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAppSelector } from 'app/hooks'

const Navigation: FC = () => {
  const { registered } = useAppSelector((state) => state.traffic)
  const navigate = useNavigate()

  return (
    <Header background="dark-1" pad="medium">
      <Heading color="white" size="small">
        Traffic Zen
      </Heading>
      <Nav direction="row">
        {registered && (
          <Button
            icon={<Configure />}
            gap="xlarge"
            onClick={() => navigate('/settings')}
          />
        )}
      </Nav>
    </Header>
  )
}

export default Navigation
