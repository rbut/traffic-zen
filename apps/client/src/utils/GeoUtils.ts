/**
 * Format distance to a nice looking string.
 * @param distance - distance to format
 * @returns formatted string.
 */
export function formatDistance(distance: number): string {
  return `${distance.toFixed(1).toLocaleString()} km`
}

/**
 * Get a color based on incident priority.
 * @param priority - the priority.
 * @returns priority color.
 */
export function priorityToColor(priority: number): string {
  switch (priority) {
    case 1:
      return 'status-ok'
    case 2:
      return 'status-error'
    case 3:
      return 'status-warning'
    case 4:
      return 'status-warning'
    case 5:
      return 'status-critical'
    default:
      return 'status-unknown'
  }
}

/**
 * Convert a category code to string.
 * @param category - the category code.
 * @returns category string.
 */
export function categoryToString(category: number): string {
  switch (category) {
    case 0:
      return 'Road traffic'
    case 1:
      return 'Public transport'
    case 2:
      return 'Planned disturbance'
    case 3:
      return 'Other'
    default:
      return 'Unknown'
  }
}
