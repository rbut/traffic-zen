import { Box, Grid } from 'grommet'
import { FC, useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import StartPage from './pages/StartPage'
import IncidentPage from './pages/IncidentPage'
import SettingsPage from './pages/SettingsPage'
import Navigation from './components/Navigation'
import { useAppDispatch, useAppSelector } from 'app/hooks'
import { connect, register } from 'app/reducers/trafficSlice'

const App: FC = () => {
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(connect())
  }, [dispatch])

  const { lat, lon } = useAppSelector((state) => state.geolocation)
  const connected = useAppSelector((state) => state.traffic.connected)

  // register when connected and position is updated.
  useEffect(() => {
    if (connected && lat && lon) {
      dispatch(register({ lat, lon }))
    }
  }, [connected, lat, lon, dispatch])

  return (
    <BrowserRouter>
      <Grid fill rows={['auto', 'flex']}>
        <Navigation />
        <Box overflow="auto" width="full" background="light-3">
          <Routes>
            <Route path="/incident/:id" element={<IncidentPage />} />
            <Route path="/settings" element={<SettingsPage />} />
            <Route path="/" element={<StartPage />} />
          </Routes>
        </Box>
      </Grid>
    </BrowserRouter>
  )
}

export default App
