import { FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Box, Button, Card, PageHeader, Spinner, Text } from 'grommet'
import { CircleAlert, Command } from 'grommet-icons'
import {
  formatDistance,
  priorityToColor,
  categoryToString,
} from 'utils/GeoUtils'
import { haversineDistance } from '@traffic-zen/common/utils/GeoUtils'
import { useAppSelector } from 'app/hooks'

const IncidentPage: FC = () => {
  const { id } = useParams()
  const navigate = useNavigate()
  const { lat, lon } = useAppSelector((state) => state.geolocation)
  const { connected, incidents } = useAppSelector((state) => state.traffic)

  if (!connected) {
    return (
      <Box pad="medium" gap="medium" flex="grow">
        <Spinner />
      </Box>
    )
  }

  const incident = incidents.find(
    (incident) => incident.id === (id && parseInt(id))
  )

  if (!incident || !lat || !lon) {
    return (
      <Box pad="medium" gap="medium" flex="grow">
        <Button label="Back" onClick={() => navigate(-1)} />

        {!incident && 'Incident not found :('}
        {!lat || (!lon && 'Invalid location')}
      </Box>
    )
  }

  const distance = haversineDistance(
    lat,
    lon,
    incident.latitude,
    incident.longitude
  )

  const offset = 0.001 // magic number based on experimentation
  const osmEmbedUrl = `https://www.openstreetmap.org/export/embed.html?bbox=${
    incident.longitude - offset
  },${incident.latitude - offset},${incident.longitude + offset},${
    incident.latitude + offset
  }&layer=mapnik&marker=${incident.latitude},${incident.longitude}`

  return (
    <Box pad="medium" gap="medium" flex="grow">
      <PageHeader
        title={incident.title}
        actions={<Button label="Back" onClick={() => navigate(-1)} />}
      />

      <Box direction="row" align="center" gap="small">
        <CircleAlert size="small" color={priorityToColor(incident.priority)} />
        <Text>{formatDistance(distance)} away</Text>

        <Command size="small" color="brand" />
        <Text>{categoryToString(incident.category)}</Text>
      </Box>

      <Card
        width="full"
        background="light-1"
        pad="medium"
        margin={{ bottom: 'medium' }}
      >
        <Text size="large">{incident.description}</Text>
      </Card>

      <iframe
        frameBorder="0"
        title="OpenStreetMap"
        width="100%"
        height="400px"
        src={osmEmbedUrl}
      />
    </Box>
  )
}

export default IncidentPage
