import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { Box, Button, PageHeader, Text } from 'grommet'
import { useAppDispatch } from 'app/hooks'
import { forgetMe, getCurrentPosition } from 'app/reducers/geolocationSlice'
import { unregister } from 'app/reducers/trafficSlice'

const SettingsPage: FC = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  return (
    <Box pad="medium" gap="medium" flex="grow">
      <PageHeader
        title="Settings"
        actions={<Button label="Back" onClick={() => navigate(-1)} />}
      />

      <Button
        label="Update my position"
        primary
        onClick={() => dispatch(getCurrentPosition())}
      />
      <Text>Update the current position you're monitoring.</Text>

      <Button
        label="Forget about my location"
        primary
        onClick={() => {
          dispatch(forgetMe())
          dispatch(unregister())
          navigate('/', { replace: true })
        }}
      />
      <Text>Remove position data. This will leave the settings page.</Text>
    </Box>
  )
}

export default SettingsPage
