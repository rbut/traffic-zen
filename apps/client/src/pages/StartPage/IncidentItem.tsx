import { FC } from 'react'
import { Box, Card, Heading, Text } from 'grommet'
import { CircleAlert, Command } from 'grommet-icons'
import {
  formatDistance,
  priorityToColor,
  categoryToString,
} from 'utils/GeoUtils'
import { haversineDistance } from '@traffic-zen/common/utils/GeoUtils'
import { Incident } from '@traffic-zen/common/types'

type Props = {
  lat: number
  lon: number
  incident: Incident
  onClick: () => void
}

const IncidentItem: FC<Props> = ({ lat, lon, incident, onClick }) => {
  const distance = haversineDistance(
    lat,
    lon,
    incident.latitude,
    incident.longitude
  )

  return (
    <Card width="full" background="light-1" pad="medium" onClick={onClick}>
      <Heading size="small">{incident.title}</Heading>

      <Box direction="row" align="center" gap="small">
        <CircleAlert size="small" color={priorityToColor(incident.priority)} />
        <Text>{formatDistance(distance)} away</Text>

        <Command size="small" color="brand" />
        <Text>{categoryToString(incident.category)}</Text>
      </Box>
    </Card>
  )
}

export default IncidentItem
