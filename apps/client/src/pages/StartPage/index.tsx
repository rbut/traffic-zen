import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { Box, Button, Heading, Spinner, Text } from 'grommet'
import { Bike } from 'grommet-icons'
import { Incident } from '@traffic-zen/common/types'
import { getCurrentPosition } from 'app/reducers/geolocationSlice'
import { useAppDispatch, useAppSelector } from 'app/hooks'
import IncidentItem from './IncidentItem'

const StartPage: FC = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const { lat, lon } = useAppSelector((state) => state.geolocation)
  const trafficData = useAppSelector((state) => state.traffic)

  const renderConnecting = () => (
    <Box width="full" height="full" align="center" justify="center" gap="large">
      <Spinner />
      <Text textAlign="center">Attempting to connect to server ...</Text>
    </Box>
  )

  const renderRegister = () => (
    <Box width="full" height="full" align="center" justify="center" gap="large">
      <Heading>Welcome!</Heading>
      <Button
        label="Update my position"
        primary
        onClick={() => dispatch(getCurrentPosition())}
      />
      <Text textAlign="center">
        Please allow the permission request for your geolocation position.
      </Text>
    </Box>
  )

  const renderIncidents = (incidents: Incident[], lat: number, lon: number) =>
    incidents.map((incident) => (
      <IncidentItem
        key={incident.id}
        lat={lat}
        lon={lon}
        incident={incident}
        onClick={() => navigate(`/incident/${incident.id}`)}
      />
    ))

  const renderNoIncidents = () => (
    <Box width="full" height="full" align="center" justify="center" gap="large">
      <Bike size="large" color="status-ok" />
      <Text textAlign="center">
        No traffic incidents in your area,
        <br />
        please enjoy your commute :)
      </Text>
    </Box>
  )

  const renderWaitingForIncidents = () => (
    <Box width="full" height="full" align="center" justify="center" gap="large">
      <Spinner />
      <Text textAlign="center">Waiting for incidents ...</Text>
    </Box>
  )

  return (
    <Box pad="medium" gap="medium" flex="grow">
      {(() => {
        if (!trafficData.connected) {
          return renderConnecting()
        } else if (lat === null || lon === null) {
          return renderRegister()
        } else if (trafficData.incidents.length > 0) {
          return renderIncidents(trafficData.incidents, lat, lon)
        } else if (!trafficData.registered) {
          return renderWaitingForIncidents()
        } else {
          return renderNoIncidents()
        }
      })()}
    </Box>
  )
}

export default StartPage
