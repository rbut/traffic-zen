import { Middleware } from '@reduxjs/toolkit'
import {
  connect,
  connected,
  incidents,
  register,
} from 'app/reducers/trafficSlice'
import { haversineDistance } from '@traffic-zen/common/utils/GeoUtils'
import json from 'messages.json'

const trafficMiddleware: Middleware = (store) => {
  console.log('Running trafficMiddleware in mock mode')

  return (next) => (action) => {
    if (connect.match(action)) {
      store.dispatch(connected(true))
    }

    if (register.match(action)) {
      const results = json.messages.filter(
        (incident) =>
          haversineDistance(
            action.payload.lat,
            action.payload.lon,
            incident.latitude,
            incident.longitude
          ) < 100
      )
      store.dispatch(incidents(results))
    }

    next(action)
  }
}

export default trafficMiddleware
