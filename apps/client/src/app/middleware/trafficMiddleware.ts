import {
  Dispatch,
  TypedStartListening,
  createListenerMiddleware,
  isAnyOf,
} from '@reduxjs/toolkit'
import {
  connect,
  connected,
  disconnect,
  incidents,
  reconnect,
  register,
  unregister,
} from 'app/reducers/trafficSlice'
import { Command, Incident } from '@traffic-zen/common/types'
import { commandSchema, incidentsSchema } from '@traffic-zen/common/schemas'
import { CmdsIn, CmdsOut } from '@traffic-zen/common/types'
import { validatorFactory } from '@traffic-zen/common/utils'
import type { RootState, AppDispatch } from 'app/store'

const commandFactory = validatorFactory<Command>(commandSchema)
const incidentsFactory = validatorFactory<Incident[]>(incidentsSchema)

const listenerMiddleware = createListenerMiddleware()

type AppStartListening = TypedStartListening<RootState, AppDispatch>
const startAppListening = listenerMiddleware.startListening as AppStartListening

let socket: WebSocket | null = null
const url = import.meta.env.VITE_SERVICE_URL

const onMessage = (dispatch: Dispatch, message: MessageEvent) => {
  console.log('Received message:', message)
  const json = JSON.parse(message.data)

  try {
    const command = commandFactory.verify(json)

    switch (command.cmd) {
      case CmdsOut.INCIDENTS: {
        const data = incidentsFactory.verify(command.value)
        dispatch(incidents(data))
        break
      }
    }
  } catch (e) {
    console.error('Error in received websocket command', e)
  }
}

const onClose = (dispatch: Dispatch, ev: CloseEvent) => {
  console.warn('Disconnected from server', ev)
  dispatch(connected(false))

  if (ev.code !== 1005) {
    console.log('Socket closed unexpectedly', ev)
    dispatch(reconnect())
  }
}

const onOpen = (dispatch: Dispatch) => {
  console.log('Connected to', url)
  dispatch(connected(true))
}

startAppListening({
  actionCreator: connect,
  effect: async (_, listenerApi) => {
    listenerApi.cancelActiveListeners()
    listenerApi.unsubscribe()

    socket?.close()

    console.log('Connecting to', url, '...')
    socket = new WebSocket(url)

    socket.onopen = () => onOpen(listenerApi.dispatch)
    socket.onclose = (ev) => onClose(listenerApi.dispatch, ev)
    socket.onmessage = (message) => onMessage(listenerApi.dispatch, message)

    const reconnectTask = listenerApi.fork(async (forkApi) => {
      await listenerApi.take(reconnect.match)
      await forkApi.delay(5_000)
      listenerApi.subscribe()
      listenerApi.dispatch(connect())
    })

    await listenerApi.condition(disconnect.match)
    reconnectTask.cancel()

    console.log('Disconnecting from', url)
    socket?.close()
    socket = null

    listenerApi.subscribe()
  },
})

startAppListening({
  matcher: isAnyOf(register, unregister),
  effect: async (action, listenerApi) => {
    const { connected } = listenerApi.getState().traffic

    if (!connected) {
      console.warn(
        `Ignoring traffic action ${action.type}, not connected to server`
      )
      return
    }

    switch (action.type) {
      case register.type:
        socket?.send(
          JSON.stringify({ cmd: CmdsIn.REGISTER, value: action.payload })
        )
        break
      case unregister.type:
        socket?.send(JSON.stringify({ cmd: CmdsIn.UNREGISTER }))
        break
    }
  },
})

export default listenerMiddleware.middleware
