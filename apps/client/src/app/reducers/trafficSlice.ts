import { PayloadAction, createAction, createSlice } from '@reduxjs/toolkit'
import { Incident } from '@traffic-zen/common/types'
import { forgetMe } from './geolocationSlice'

interface TrafficState {
  connected: boolean
  registered: boolean
  incidents: Incident[]
}

const initialState: TrafficState = {
  connected: false,
  registered: false,
  incidents: [],
}

const trafficSlice = createSlice({
  name: 'traffic',
  initialState,
  reducers: {
    connected: (state, action: PayloadAction<boolean>) => {
      if (!action.payload) {
        state.registered = false
      }
      state.connected = action.payload
    },
    incidents: (state, action: PayloadAction<Incident[]>) => {
      state.registered = true
      state.incidents = action.payload
    },
  },
  extraReducers: (builder) => {
    builder.addCase(forgetMe.type, (state) => {
      state.registered = false
      state.incidents = []
    })
  },
})

export const connect = createAction('traffic/connect')
export const disconnect = createAction('traffic/disconnect')
export const reconnect = createAction('traffic/reconnect')
export const register = createAction<{ lat: number; lon: number }>(
  'traffic/register'
)
export const unregister = createAction('traffic/unregister')

export const { connected, incidents } = trafficSlice.actions

export default trafficSlice.reducer
