import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

export const REQUEST_POSITION = 'geolocation/request_position'

interface GeolocationState {
  lat: number | null
  lon: number | null
  error: boolean
}

const initialState: GeolocationState = {
  lat: null,
  lon: null,
  error: false,
}

const getCurrentPositionAsync = async (): Promise<GeolocationPosition> =>
  await new Promise((resolve: PositionCallback, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject)
  })

export const getCurrentPosition = createAsyncThunk(
  REQUEST_POSITION,
  async () => {
    const { coords } = await getCurrentPositionAsync()

    console.log('Got geolocation', coords)

    return {
      lat: coords.latitude,
      lon: coords.longitude,
    }
  }
)

export const geolocationSlice = createSlice({
  name: 'geolocation',
  initialState,
  reducers: {
    forgetMe: (state) => {
      state.lat = null
      state.lon = null
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCurrentPosition.fulfilled, (state, action) => {
        state.lat = action.payload.lat
        state.lon = action.payload.lon
        state.error = false
      })
      .addCase(getCurrentPosition.rejected, (state) => {
        state.error = true
      })
  },
})

export const { forgetMe } = geolocationSlice.actions

export default geolocationSlice.reducer
