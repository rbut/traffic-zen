import {
  combineReducers,
  configureStore,
  ThunkAction,
  Action,
  Middleware,
} from '@reduxjs/toolkit'

import geolocationReducer from './reducers/geolocationSlice'
import trafficReducer from './reducers/trafficSlice'

let trafficMiddleware: Middleware

// start traffic saga in mocked mode if variable is set, note: env variables are strings
if (import.meta.env.VITE_MOCK_TRAFFIC === 'true') {
  const module = await import('app/middleware/trafficMiddlewareMock')
  trafficMiddleware = module.default
} else {
  const module = await import('app/middleware/trafficMiddleware')
  trafficMiddleware = module.default
}

const reducers = combineReducers({
  geolocation: geolocationReducer,
  traffic: trafficReducer,
})

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().prepend(trafficMiddleware),
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof reducers>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
