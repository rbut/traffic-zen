import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'

import { Provider } from 'react-redux'
import { store } from './app/store'

import { Grommet } from 'grommet'
import theme from './theme'
import { GlobalStyle } from './main.styled'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Grommet theme={theme} full>
      <Provider store={store}>
        <GlobalStyle />
        <App />
      </Provider>
    </Grommet>
  </React.StrictMode>
)
