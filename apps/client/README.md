# Traffic Zen Client

Frontend for displaying data from Traffic Zen service.

## Environment options

The following configuration options are available in the .env file.

`VITE_SERVICE_URL` sets the path to the Traffic Zen service endpoint. Example: `VITE_SERVICE_URL=ws://localhost:8080`

`VITE_MOCK_TRAFFIC` can be set to true to enable mock implementation of the connection to the service. Example: `VITE_APP_MOCK_TRAFFIC=false`
