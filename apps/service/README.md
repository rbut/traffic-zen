# Traffic Zen Service

Service that provides traffic data to registered clients over Websocket connections. Defaults to port 8080, but this can be changed by passing in the environment variable PORT to the process.

## Requirements

- Node.js LTS (16.x)
