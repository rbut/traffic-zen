import { filterIncidents } from './TrafficController'
import { readFileSync } from 'node:fs'
import { describe, expect, it } from 'vitest'

describe('TrafficController tests', () => {
  it('filter incidents in Malmo area 10km radius', () => {
    const malmoLat = 55.60587
    const malmoLon = 13.00073
    const maxRadius = 10

    const blob = readFileSync('./apps/service/sample/messages.json', {
      encoding: 'utf8',
      flag: 'r',
    })
    const json = JSON.parse(blob)

    const results = filterIncidents(
      json.messages,
      maxRadius,
      malmoLat,
      malmoLon
    )

    expect(results.length).to.equal(2)
  })

  it('filter incidents in Malmo area 1600km radius', () => {
    const malmoLat = 55.60587
    const malmoLon = 13.00073
    const maxRadius = 1600 // about length of Sweden, should return all the results

    const blob = readFileSync('./apps/service/sample/messages.json', {
      encoding: 'utf8',
      flag: 'r',
    })
    const json = JSON.parse(blob)

    const results = filterIncidents(
      json.messages,
      maxRadius,
      malmoLat,
      malmoLon
    )

    expect(results.length).to.equal(json.messages.length)
  })
})
