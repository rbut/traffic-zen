import { WebSocketServer, WebSocket } from 'ws'
import { v4 as uuidv4 } from 'uuid'
import { Type } from '@sinclair/typebox'
import { haversineDistance } from '@traffic-zen/common/utils/GeoUtils'
import { CmdsIn, CmdsOut, Incident } from '@traffic-zen/common/types'
import { validatorFactory } from '@traffic-zen/common/utils'
import { Command } from '@traffic-zen/common/types'
import { commandSchema } from '@traffic-zen/common/schemas'

const MAX_INCIDENT_RADIUS = 10
const PONG_INTERVAL = 30_000

// JSON validation
const commandFactory = validatorFactory<Command>(commandSchema)

const registerSchema = Type.Object(
  {
    lat: Type.Number(),
    lon: Type.Number(),
  },
  { additionalProperties: false }
)
const registerFactory = validatorFactory<Coordinates>(registerSchema)

interface WebSocketServerWithClientId extends WebSocketServer {
  clients: Set<WebSocketWithClientId>
}

interface WebSocketWithClientId extends WebSocket {
  clientId: string
  isAlive: boolean
}

interface Coordinates {
  lat: number
  lon: number
}

/**
 * Class for setting up a websocket server
 * and communicating with clients.
 * @class
 */
export default class TrafficController {
  private _clients: { [key: string]: Coordinates }
  private _incidents: Incident[]
  private _intervalId: NodeJS.Timeout | number
  private _wss: WebSocketServerWithClientId

  /**
   * @param port - the server port to listen on.
   */
  constructor(port: number) {
    this._clients = {}
    this._incidents = []
    this._intervalId = -1
    this._wss = new WebSocketServer({ port }) as WebSocketServerWithClientId

    this._wss.on('connection', (ws: WebSocketWithClientId, req) => {
      ws.clientId = uuidv4()
      ws.isAlive = true

      console.log(
        `Client connected: ${req.socket.remoteAddress} | ${ws.clientId}`
      )

      ws.on('pong', () => {
        ws.isAlive = true
      })

      ws.on('message', (message, isBinary) => {
        try {
          !isBinary && this.processMessage(ws, message.toString())
        } catch (e: unknown) {
          console.warn('Message from client is not valid:', e)
        }
      })

      ws.on('close', (reason) => {
        console.log(
          `Client disconnected : ${req.socket.remoteAddress} | reason: ${reason}`
        )
        this.unregisterClient(ws)
      })
    })

    this._intervalId = setInterval(() => {
      this._wss.clients.forEach((ws) => {
        if (!ws.isAlive) return ws.terminate()

        ws.isAlive = false
        ws.ping()
      })
    }, PONG_INTERVAL)

    this._wss.on('close', () => {
      clearInterval(this._intervalId)
    })
  }

  /**
   * Broadcast incidents to registered clients.
   * @param incidents - list of incidents.
   */
  public broadcastIncidents(incidents: Incident[]) {
    // save messages for future clients
    this._incidents = incidents

    this._wss.clients.forEach((ws) => {
      if (ws.readyState !== WebSocket.OPEN) {
        return
      }

      const client = this._clients[ws.clientId]
      if (!client) {
        return
      }
      const msg = {
        cmd: CmdsOut.INCIDENTS,
        value: filterIncidents(
          this._incidents,
          MAX_INCIDENT_RADIUS,
          client.lat,
          client.lon
        ),
      }
      ws.send(JSON.stringify(msg))
    })
  }

  // Will throw an error if message is not formatted properly.
  private processMessage(ws: WebSocketWithClientId, message: string) {
    const msgObj = JSON.parse(message)

    try {
      const command = commandFactory.verify(msgObj)

      switch (command.cmd) {
        case CmdsIn.REGISTER: {
          const coordinates = registerFactory.verify(msgObj.value)
          this.registerClient(ws, coordinates)
          break
        }
        case CmdsIn.UNREGISTER: {
          this.unregisterClient(ws)
          break
        }
      }
    } catch (e) {
      console.warn('JSON cmd parse error:', e)
      throw Error('JSON cmd parse error')
    }
  }

  private registerClient(ws: WebSocketWithClientId, value: Coordinates) {
    console.log(`Client registered: ${ws.clientId}`)

    this._clients[ws.clientId] = { ...value }

    // broadcast current incidents to newly registered client
    const msg = {
      cmd: CmdsOut.INCIDENTS,
      value: filterIncidents(
        this._incidents,
        MAX_INCIDENT_RADIUS,
        value.lat,
        value.lon
      ),
    }
    ws.send(JSON.stringify(msg))
  }

  private unregisterClient(ws: WebSocketWithClientId) {
    if (ws) {
      console.log(`Client unregistered: ${ws.clientId}`)
      delete this._clients[ws.clientId]
    }
  }
}

/**
 * Filter incidents within allowed radius.
 * @param incidents - list of all incidents.
 * @param maxRadius - max allowed radius for incidents in km.
 * @param lat - latitude of client.
 * @param lon - longitude of client.
 * @returns list of incidents within allowed radius.
 */
export function filterIncidents(
  incidents: Incident[],
  maxRadius: number,
  lat: number,
  lon: number
): Incident[] {
  return incidents.filter((incident) => {
    const distanceToIncident = haversineDistance(
      incident.latitude,
      incident.longitude,
      lat,
      lon
    )

    return distanceToIncident < maxRadius
  })
}
