import axios, { CancelToken, CancelTokenSource } from 'axios'
import { EventEmitter } from 'node:events'
import { incidentsSchema } from '@traffic-zen/common/schemas'
import { Incident } from '@traffic-zen/common/types'
import { validatorFactory } from '@traffic-zen/common/utils'

// Assume there will be no more than 1000 messages.
const API_SOURCE =
  'https://api.sr.se/api/v2/traffic/messages?format=json&size=1000'

// Update data every 15 minutes.
const POLL_RATE = 1_000 * 60 * 15

declare interface SrApiService {
  on(event: 'incidents', listener: (incidents: Incident[]) => void): this
}

const incidentsFactory = validatorFactory<Incident[]>(incidentsSchema)

/**
 * Class that fetches traffic incidents from sr.se.
 * @class
 */
class SrApiService extends EventEmitter {
  private _cancelSource: CancelTokenSource
  private _intervalId: NodeJS.Timeout | number

  constructor() {
    super()
    this._cancelSource = getCancelSource()
    this._intervalId = -1
  }

  /**
   * Start polling for incidents.
   * Results are published on the emitter's 'incidents' channel.
   */
  public start() {
    const poll = async () => {
      this._cancelSource.cancel()
      this._cancelSource = getCancelSource()
      const incidents = await getIncidents(this._cancelSource.token)

      this.emit('incidents', incidents)
    }
    poll()

    this._intervalId = setInterval(poll, POLL_RATE)
  }

  /**
   * Stop polling for incidents.
   */
  public stop() {
    this._cancelSource.cancel()
    clearInterval(this._intervalId)
  }
}

export default SrApiService

/**
 * Get incidents from API endpoint.
 * @param cancelToken - token for cancelling request.
 * @returns promise with array of incidents.
 */
async function getIncidents(cancelToken: CancelToken) {
  try {
    console.info(`Fetching new incidents from ${API_SOURCE} ...`)

    const response = await axios.get(API_SOURCE, {
      responseType: 'json',
      cancelToken,
    })

    try {
      const incidents = incidentsFactory.verify(response.data.messages)
      console.info(`Got ${incidents.length} incidents back`)

      return incidents
    } catch (e) {
      console.error('Unable to to validate SR response data:', e)
      return []
    }
  } catch (e) {
    console.error('Unable to fetch traffic incidents', e)
    return []
  }
}

/**
 * Get a source for cancelling axios request.
 * @returns a cancel token source.
 */
function getCancelSource(): CancelTokenSource {
  const CancelToken = axios.CancelToken
  return CancelToken.source()
}
