import TrafficController from './controllers/TrafficController'
import SrApiProvider from './providers/SrApiProvider'

const srApi = new SrApiProvider()
const traffic = new TrafficController(
  (process.env.PORT && parseInt(process.env.PORT)) || 8080
)

console.log('### Traffic Zen Service ###')

srApi.on('incidents', (incidents) => {
  traffic.broadcastIncidents(incidents)
})
srApi.start()

process.on('SIGTERM', () => {
  srApi.stop()
})
