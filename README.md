# Traffic Zen

## Full stack flow

- Service starts a websocket server and fetches SR.se traffic data every 15th minutes.
- Client connects and registers with the server, providing it's current position.
- Service responds with traffic incidents within range.
- The service will notify the connected and registered clients of updated incidents within their position.

## Requirements

- Node.js LTS (16.x)
- pnpm (https://pnpm.io/installation)

### Getting started

`pnpm install`
`pnpm dev`
Open `http://localhost:5173/` in the browser of your choice.
